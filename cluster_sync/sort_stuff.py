# !/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import shutil
from tqdm import tqdm

'''
sorts files from source dir to corresponding LExx folder (by copying, not moving)
any new sorting scheme that do not include filenames starting with 'LEXX....' wont work
idk whether it overwrites if file already exists
'''
basedir = '/home/jordi/Documents/changes_of_mind/data/'
source_dir = '/home/jordi/Documents/changes_of_mind/data/to_sort/'
#os.path.abspath('./to_sort') # or mounted cluster folder (never tested)

# now with videos which have been already analyzed in '/home/jordi/Documents/changes_of_mind/data/pending_videos/'
# in this case we just want to move those videos which have been analized. Start with .h5 files and includingmetadata.pickle; then copy the video and the time vector

pending_dir = '/home/jordi/Documents/changes_of_mind/data/pending_videos/'
suffix = 'DeepCut_resnet50_metamix2019Jul03shuffle1_1030000.h5'

biglist = os.listdir(pending_dir)
targets = [x[:-(len(suffix))] for x in biglist if x.endswith(suffix)]

files_to_move = []

for target in targets:
    files_to_move += [x for x in biglist if x.startswith(target)]

for f in files_to_move:
    shutil.move(pending_dir+f, source_dir+f)

files_to_move = [x for x in os.listdir(pending_dir) if x.endswith('.npy')]
for f in files_to_move:
    shutil.move(pending_dir+f, source_dir+f)

target_csv = []
target_npy = []
target_avi = []
target_h5 = []
target_pickle = []

# add video placeholders?

for dirpath, dirnames, filenames in os.walk(source_dir): # crawl

    target_csv += [os.path.join(dirpath,x) for x in filenames if x.endswith('.csv')]
    target_npy += [os.path.join(dirpath,x) for x in filenames if x.endswith('.npy')]
    target_avi += [os.path.join(dirpath,x) for x in filenames if x.endswith('.avi')]
    target_h5 += [os.path.join(dirpath,x) for x in filenames if x.endswith('.h5')]
    target_pickle += [os.path.join(dirpath,x) for x in filenames if x.endswith('.pickle')]



# same block for each filetype
for item in tqdm(target_csv, desc='csvs'):
    sdirname ,filename = os.path.split(item)
    tdirname = basedir+filename.split('_')[0]+'/sessions/' # eg './LE23/sessions/'
    # ensure filepath exists
    if not os.path.exists(basedir+filename.split('_')[0]):
        os.makedirs(basedir+filename.split('_')[0])
    if not os.path.exists(tdirname):
        os.makedirs(tdirname)
    
    shutil.move(item, os.path.join(tdirname, filename))

for item in tqdm(target_npy, desc='timestamps'):
    sdirname ,filename = os.path.split(item)
    tdirname = basedir+filename.split('_')[0]+'/videos/'
    # ensure filepath exists
    if not os.path.exists(basedir+filename.split('_')[0]):
        os.makedirs(basedir+filename.split('_')[0])
    if not os.path.exists(tdirname):
        os.makedirs(tdirname)

    shutil.move(item, os.path.join(tdirname, filename))

for item in tqdm(target_avi, desc='videos'):
    sdirname ,filename = os.path.split(item)
    tdirname = basedir+filename.split('_')[0]+'/videos/'
    # ensure filepath exists
    if not os.path.exists(basedir+filename.split('_')[0]):
        os.makedirs(basedir+filename.split('_')[0])
    if not os.path.exists(tdirname):
        os.makedirs(tdirname)

    shutil.move(item, os.path.join(tdirname, filename))

for item in tqdm(target_h5, desc='trajectories'):
    sdirname ,filename = os.path.split(item)
    tdirname = basedir+filename.split('_')[0]+'/poses/'
    # ensure filepath exists
    if not os.path.exists(basedir+filename.split('_')[0]):
        os.makedirs(basedir+filename.split('_')[0])
    if not os.path.exists(tdirname):
        os.makedirs(tdirname)

    shutil.move(item, os.path.join(tdirname, filename))

for item in tqdm(target_pickle, desc='video meta'):
    sdirname ,filename = os.path.split(item)
    tdirname = basedir+filename.split('_')[0]+'/poses/'
    # ensure filepath exists
    if not os.path.exists(basedir+filename.split('_')[0]):
        os.makedirs(basedir+filename.split('_')[0])
    if not os.path.exists(tdirname):
        os.makedirs(tdirname)

    shutil.move(item, os.path.join(tdirname, filename))







print('done ~=[,,_,,]:3')
