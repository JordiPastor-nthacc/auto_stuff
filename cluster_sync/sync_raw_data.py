import pandas as pd
import numpy as np
import os
import shutil
from tqdm import tqdm
from user_keys import keys

tracked_subjects = ['LE'+str(x) for x in range(36, 48)]
local_tosort_path = '/home/jordi/Documents/changes_of_mind/data/to_sort/'
local_pending_vid = '/home/jordi/Documents/changes_of_mind/data/pending_videos/'


print('mounting archive to local fs')
#os.system()
# after removing securecheck for keys in remote machine .,,.
# mount:
# sshfs jpastor@neurocomp.fcrb.es:/ /home/jordi/DATA/mountedCluster/
# or adding a password: echo 'password' | sshfs user@host:/ /home/jordi/DATA/mountedCluster/
os.system(f'echo {keys['password']} | sshfs {keys['user']}@{keys['server']}:/ /home/jordi/DATA/mountedCluster/ -o password_stdin')
# unmount:
# fusermount -u /home/jordi/DATA/mountedCluster

# get blacklisted sessions, get it manually

print('listing local data')

unsorted = os.listdir(local_tosort_path)

unsortedvideos = [x for x in unsorted if x.endswith('.avi')]
unsortednpy = [x for x in unsorted if x.endswith('.npy')]
unsortedcsv = [x for x in unsorted if x.endswith('.csv')]

# get current local data

localvideos, localtimestamps, localsessions = {}, {}, {}
for subj in tqdm(tracked_subjects):
    templist_vid = [x for x in os.listdir(f'/home/jordi/Documents/changes_of_mind/data/{subj}/videos/') if x.endswith('.avi')] #(x.endswith('.avi') and '_p4_' in x)]
    templist_npy = [x for x in os.listdir(f'/home/jordi/Documents/changes_of_mind/data/{subj}/videos/') if x.endswith('.npy')] # (x.endswith('.npy') and '_p4_' in x)]
    templist_csv = [x for x in os.listdir(f'/home/jordi/Documents/changes_of_mind/data/{subj}/sessions/') if x.endswith('.csv')] #(x.endswith('.csv') and '_p4_' in x)]

    # add corresponding unsorted
    templist_vid += [x for x in unsortedvideos if x.startswith(subj)]
    templist_npy += [x for x in unsortednpy if x.startswith(subj)]
    templist_csv += [x for x in unsortedcsv if x.startswith(subj)]

    for dic, l in zip([localvideos, localtimestamps, localsessions],[templist_vid, templist_npy, templist_csv]):
        dic[subj] = l



# print(dic)
# blacklist 'errorvid' folders # ~~ common without extensions?
blacklisted_video = os.listdir('/home/jordi/Documents/changes_of_mind/data/pending_videos/errorvid') + os.listdir('/home/jordi/Documents/changes_of_mind/data/pending_videos/notyet_vid')
for subj in tracked_subjects:
    if os.path.exists(f'/home/jordi/Documents/changes_of_mind/data/{subj}/videos/errorvid'):
        blacklisted_video += os.listdir(f'/home/jordi/Documents/changes_of_mind/data/{subj}/videos/errorvid')


sessions_to_copy = []
timestamps_to_copy = []
videos_to_copy = []

print('listing remote data')
#remotevideos, remotetimestamps, remotesessions  = {}, {}, {}
for subj in tqdm(tracked_subjects):
    cluster_video_path = f'/home/jordi/DATA/mountedCluster/archive/rat/videos/{subj}/'
    for dirpath, dirnames, filenames in os.walk(cluster_video_path):
        videos_to_copy += [os.path.join(dirpath, x) for x in filenames if (x.endswith('.avi') and ('_p4_' in x) and (x not in localvideos[subj]) and (x not in blacklisted_video))]
        timestamps_to_copy += [os.path.join(dirpath, x) for x in filenames if (x.endswith('.npy') and ('_p4_' in x) and (x not in localtimestamps[subj]))]
    
    cluster_session_path = f'/home/jordi/DATA/mountedCluster/archive/rat/behavioral_data/{subj.lower()}/sessions/'
    for dirpath, dirnames, filenames in os.walk(cluster_session_path):
        sessions_to_copy += [os.path.join(dirpath, x) for x in filenames if (x.endswith('.csv') and ('_p4_' in x) and (x not in localsessions[subj]))]


print('dlading stuff, it may take a while')


for item in tqdm(sessions_to_copy, desc='csvs'):
    _, fname = os.path.split(item)
    shutil.copyfile(item, local_tosort_path+fname)
    #print(item)

for item in tqdm(timestamps_to_copy, desc='timestamps'):
    _, fname = os.path.split(item)
    shutil.copyfile(item, local_tosort_path+fname)
    #print(item)

for item in tqdm(videos_to_copy, desc='videos'):
    _, fname = os.path.split(item)
    shutil.copyfile(item, local_pending_vid+fname)
    #print(item)

# umount
os.system('fusermount -u /home/jordi/DATA/mountedCluster')