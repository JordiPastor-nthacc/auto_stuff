# !/usr/bin/python3
# -*- coding: utf-8 -*-

'''
keys should be ignored now: git update-index --assume-unchanged [path]
Automating the hassle one step further including some code found here: https://www.geeksforgeeks.org/send-mail-attachment-gmail-account-using-python/
'''
import os
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime
from ftplib import FTP
import shutil
from spam_config import mail_stuff, server1, server2
import smtplib 
from email.mime.multipart import MIMEMultipart 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email import encoders
from tqdm import tqdm

now = datetime.datetime.now()
then = now - datetime.timedelta(days=8)
print(f"execution time: {now.strftime('%Y-%m-%d %H:%M')}")
def get_plot(host, username, password, device_name):
    try:
        ftp = FTP(host)
        ftp.login(user=username, passwd=password)

        temppath = os.path.abspath('./temp') 
        if not os.path.exists(temppath):
            os.makedirs(temppath)
        pltpath = os.path.abspath('./plots') 
        if not os.path.exists(pltpath):
            os.makedirs(pltpath)

        ftp.cwd('/StorageCard/DATA')
        #ftp.retrbinary()

        filenames = ftp.nlst() # get filenames within the directory
        for filename in filenames:
            local_filename = os.path.join(temppath, filename)
            file = open(local_filename, 'wb')
            ftp.retrbinary('RETR '+ filename, file.write)


        # copy paste from now on
        files = [f for f in os.listdir(temppath) if os.path.isfile(os.path.join(temppath, f)) and f.endswith('.GDB')]

        cabinet = pd.read_csv(temppath+'/'+files[0], skiprows=9, sep='\t', parse_dates=['DD.MM.YYYY hh:mm:ss'],date_parser=lambda x: pd.to_datetime(x, format="%d.%m.%Y %H:%M.%S"))

        for i in tqdm(range(1, len(files), 1)):
            try:
                cabinet = cabinet.append(pd.read_csv(temppath+'/'+files[i], skiprows=9, sep='\t', parse_dates=['DD.MM.YYYY hh:mm:ss'],
                                 date_parser=lambda x: pd.to_datetime(x, format="%d.%m.%Y %H:%M.%S")))#, ignore_index=True)
                #print(str(i)+': rows='+str(len(cabinet))+'; last file: '+files[i])
            except:
                print('conflict in', files[i])

        cabinet.columns = ['time', 'pressure', 'RH', 'temp', 'flow', 'todrop']
        cabinet = cabinet.set_index(['time'])
        cabinet.sort_index(inplace=True)

        print(f"retrieved {device_name} data with shape {cabinet.shape}, last timepoint = {cabinet.index.values[-1]}")
        fig, ax= plt.subplots(figsize=(24, 6))
        plt.plot(cabinet[then:now]['RH'], linestyle='-', marker='o', markersize=2)
        plt.plot(cabinet[then:now]['temp'], linestyle='-', marker='o', markersize=2)
        plt.title(device_name+" since "+then.strftime('%Y-%m-%d'))
        plt.legend()
        #ax = fig.add_subplot(1,1,1)
        ax.xaxis.set_major_locator(mdates.DayLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d-%m'))
        ax.xaxis.set_minor_locator(mdates.HourLocator())
        ax.grid(which='both')
        ax.grid(which='minor', alpha=0.2)
        ax.grid(which='major', alpha=0.5)
        plt.setp(plt.gca().xaxis.get_majorticklabels(),'rotation', 90)
        fig_path = os.path.join(pltpath, now.strftime('%Y-%m-%d')+device_name+".png")
        fig.tight_layout()
        fig.savefig(fig_path, format='png')
        #plt.show()



        ftp.quit()

        # remove files in temppath
        shutil.rmtree(temppath)
        print(f"{device_name} plot done")
        return fig_path
    except:
        return False


attachment_files = []

for i, server in enumerate([server1, server2]):
    attachment_files += [get_plot(server['ip'], server['user'], server['pass'], server['name'])]

#print(attachment_files)
if any(attachment_files): # no need to attempt to send stuff if script failed before
    # code reused from here: https://www.geeksforgeeks.org/send-mail-attachment-gmail-account-using-python/

    # instance of MIMEMultipart 
    msg = MIMEMultipart() 
      
    # storing the senders email address   
    msg['From'] = mail_stuff['address'] 
      
    # storing the receivers email address  
    msg['To'] = mail_stuff['To']
    msg['Cc'] = mail_stuff['Cc']
      
    # storing the subject  
    msg['Subject'] = mail_stuff['subject_base']+f" {now.strftime('%Y-%m-%d')}"
      
    # string to store the body of the mail 
    body = f"Hi,\r\r\n\r\r\nI attached last week cabinet data, retrieved just now ({now.strftime('%I:%M%p on %B %d, %Y')}). Do not hesitate to contact us if you detect any abnormality.\r\r\n\r\r\nCheers!\r\r\nJordi"
      
    # attach the body with the msg instance 
    msg.attach(MIMEText(body, 'plain')) 
      
    # open the file to be sent  
    # filename = "File_name_with_extension"
    # attachment = open("Path of the file", "rb")   
    # instance of MIMEBase and named as p 
    # p = MIMEBase('application', 'octet-stream')   
    # To change the payload into encoded form 
    # p.set_payload((attachment).read()) 
    # encode into base64 
    # encoders.encode_base64(p) 
    # p.add_header('Content-Disposition', "attachment; filename= %s" % filename)   
    # attach the instance 'p' to instance 'msg' 
    # msg.attach(p) 
      

    for each_file_path in attachment_files:
        try:
            file_name=each_file_path.split("/")[-1]
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(each_file_path, "rb").read())

            encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
            msg.attach(part)
            print(f"{file_name} has been attached correctly")
        except:
            print("could not attach file")


    # creates SMTP session 
    s = smtplib.SMTP('smtp.gmail.com', 587) 
      
    # start TLS for security 
    s.starttls() 
      
    # Authentication 
    s.login(mail_stuff['address'], mail_stuff['password']) 
      
    # Converts the Multipart msg into a string 
    text = msg.as_string() 
      
    # sending the mail 
    s.sendmail(msg["From"], msg["To"].split(",") + msg["Cc"].split(","), text) 
      
    # terminating the session 
    s.quit() 
    print('e-mail was sentt')
else:
    print('mail section got skipped')
